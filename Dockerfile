FROM openjdk:11-jdk

### CONFIGURATION ==================================================================================

# Environment variables:
ENV ANDROID_SDK_ROOT="/android/sdk"

# Installation arguments:
ARG ANDROID_SDK_TOOLS_URL="https://dl.google.com/android/repository/commandlinetools-linux-6200805_latest.zip"

### IMAGE DEPENDENCIES & TOOLS =====================================================================

# Standard libraries:
RUN apt-get -q -y update && apt-get -y install \ 
    lib32stdc++6 \
    lib32z1 && \
    apt-get -q -y clean && rm -rf /var/lib/apt/lists/* /var/cache/apt
	
# Google Cloud SDK:
RUN apt-get -q -y update && apt-get -y install \
    python-dev \
    python-setuptools \
	python3-pip \
    apt-transport-https \
    lsb-release && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get -q -y update && apt-get -y install gcc-multilib && \
    rm -rf /var/lib/apt/lists/* && \
    pip uninstall crcmod && \
    pip install --no-cache -U crcmod

RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb https://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

RUN apt-get -q -y update && apt-get -y install google-cloud-sdk && \
    rm -rf /var/lib/apt/lists/* && \
    gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true
	
### ANDROID ========================================================================================

# Download SDK tools:
RUN set -o errexit -o nounset && \
    echo "Creating Android SDK ROOT directory" && \
    mkdir -p $ANDROID_SDK_ROOT "${ANDROID_SDK_ROOT}/cmdline-tools" .android && \
    \
    echo "Downloading Android SDK tools" && \
    cd /tmp && \
    wget -q --output-document=tools.zip $ANDROID_SDK_TOOLS_URL && \
    unzip -q tools.zip -d "${ANDROID_SDK_ROOT}/cmdline-tools" && \
    rm tools.zip

# Export environment variables for Android tools specific directories.
ENV PATH="$PATH:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin:${ANDROID_SDK_ROOT}/cmdline-tools/platform-tools:${ANDROID_SDK_ROOT}/cmdline-tools/emulator"

# Update SDK components:
RUN set -o errexit -o nounset && \
    echo "Updating Android SDK components" && \
    echo yes | sdkmanager --update && echo yes | sdkmanager "platform-tools"

# Set working directory:
WORKDIR /android

### VERIFICATION ===================================================================================

RUN set -o errexit -o nounset && \
    echo "Testing Image tools installation" && \
    git --version && \
    \
    echo "Testing Android SDK configuration" && \
    sdkmanager --list
